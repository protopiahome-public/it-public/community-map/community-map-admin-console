import React, {Component} from "react";
import layouts from "../layouts";
import { NavLink } from 'react-router-dom';
import { withRouter } from "react-router";
import CurrentUser from "../data/CurrentUser";
import {__} from "../utilities/i18n";

class LayoutMenuMain extends Component
{
	state = { isOpen : this.props.isOpen, current:this.props.current };
	componentWillReceiveProps (nextProps )
	{
		this.setState({
			current: nextProps.current,
			isOpen: nextProps.isOpen
		});
	}
	render()
	{
		let chldrn = [], grndchldrn = [], isOpen = false;
		const firstRoute = this.getParent();
		const childrenss = this.getChildren();
		if(childrenss && !firstRoute[0].hide_slider)
		{
			
			chldrn = childrenss.map((e, i) => {
				const isRole = e.capability 
					&& Array.isArray(e.capability) 
					&& CurrentUser.intersectionRole(e.capability).length==0;
				grndchldrn = this.getGrandChildren( e );
				if(isRole) return "";
				return <NavLink 
					className="list-element" 
					activeClassName="active"
					to={ "/" + this.getFirstRoute() + "/" + e.route }
					key={i}
				>			
					{__(e.title)}
					{
						grndchldrn ?
							grndchldrn.map((element, ii) => <NavLink 
								className="list-element-child" 
								activeClassName="active"
								to={ "/" + this.getFirstRoute() + "/" + e.route + "/" + element.route }
								key={ii}
							>
								{__(element.title)}
							</NavLink> )
							:
							null
					}
				</NavLink> 
			});
			isOpen = true;
		}	
		return <div className={ "layout-menu-main" +( isOpen ? " open " : "" ) } >
			{chldrn}
		</div>
	}
	getFirstRoute()
	{
		const url = this.props.location.pathname.split("/")[1];
		return url ? url : "";
	}
	getParent()
	{
		const rts = this.getFirstRoute();
		//console.log(rts);
		return layouts.menu			
			.concat(layouts.extended_routes)
				.concat(layouts.profile)
					.concat(layouts.help)
						.concat(layouts.bells)
							.concat(layouts.comments)
								.concat(layouts.main_menu)
									.filter( e => e.route === rts);
	}
	getGrandChildren( chldrn )
	{
		if(!chldrn) return false;
		//console.log(chldrn);
		if( chldrn.children && chldrn.children.length > 0 )
		{
			return chldrn.children;
		}
		else
			return false;
	}
	getChildren()
	{
		const chldrn = this.getParent();
		if(chldrn.length > 0)
		{
			//console.log( chldrn[0].children );
			if( chldrn[0].children && chldrn[0].children.length > 0 )
			{
				return chldrn[0].children;
			}
			else
				return false;
		}
		else
		{
			return false;
		}
	}
}
export default withRouter(LayoutMenuMain);